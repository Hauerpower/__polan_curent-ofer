<div <?php post_class('post-content');?>>

	<div class="grid-container">
	
		<h1 class="page-title"><?php the_title();?></h1>
		<?php the_content();?>
		
	</div>

</div>