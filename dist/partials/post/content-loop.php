<div class="loop-post <?php if( isset($ajax_load) && $ajax_load ) echo 'ajax-loaded';?>">
	<h2 class="page-title page-title--loop"><a href="<?php the_permalink()?>"><?php the_title();?></a></h2>
	<?php the_excerpt();?>
	<p><a href="<?php the_permalink()?>"><?php a_e('Więcej');?></a></p>
</div>