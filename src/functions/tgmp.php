<?php
require_once 'tgmp/class-tgm-plugin-activation.php';
add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );
function my_theme_register_required_plugins() {
	$plugins = array(
		array(
			'name' => 'ACF Pro',
			'slug' => 'advanced-custom-fields-pro',
			'source' => get_template_directory() . '/plugins/advanced-custom-fields-pro.zip', // The plugin source.
			'required' => false
		),
		array(
			'name' => 'ACF Medium Editor Field',
			'slug' => 'acf-medium-editor-field',
			'required' => false
		),
		array(
			'name' => 'YOAST SEO',
			'slug' => 'wordpress-seo',
			'required' => false
		),
		array(
			'name' => 'ACF Content Analysis for Yoast SEO',
			'slug' => 'acf-content-analysis-for-yoast-seo',
			'required' => false
		),
		array(
			'name' => 'Smush',
			'slug' => 'wp-smushit',
			'required' => false
		),
/*
		array(
			'name' => 'Fancybox',
			'slug' => 'w3dev-fancybox',
			'required' => false
		),
*/
		array(
			'name' => 'AIOWPSEC',
			'slug' => 'all-in-one-wp-security-and-firewall',
			'required' => false
		),
/*
		array(
			'name' => 'TinyMCE Advanced',
			'slug' => 'tinymce-advanced',
			'required' => false
		),
*/
		array(
			'name' => 'Sierotki',
			'slug' => 'sierotki',
			'required' => false
		),
		array(
			'name' => 'Contact Form 7',
			'slug' => 'contact-form-7',
			'required' => false
		),
		array(
			'name' => 'Odnawianie miniaturek',
			'slug' => 'ajax-thumbnail-rebuild',
			'required' => false
		),
	);
	$config = array(
		'id'           => 'tgmpa',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'plugins.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => false,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => true,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.
		/*
		'strings'      => array(
			'page_title'                      => __( 'Install Required Plugins', 'theme-slug' ),
			'menu_title'                      => __( 'Install Plugins', 'theme-slug' ),
			// <snip>...</snip>
			'nag_type'                        => 'updated', // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
		)
		*/
	);
	tgmpa( $plugins, $config );
}
